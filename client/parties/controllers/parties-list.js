angular.module('socially').controller('PartiesListCtrl', ['$scope', '$meteor', "$rootScope", 
	function ($scope, $meteor, $rootScope) {
		$scope.page = 1;
		$scope.perPage = 3;
		$scope.sort = { name: -1 };
		$scope.orderProperty = '1';
		$scope.partiesCount = 0;
		$scope.parties = $meteor.collection(function() {
			return Parties.find({}, {
				sort : $scope.getReactively("sort")
			});
		});
		$scope.users = $meteor.collection(Meteor.users, false);
		$scope.$meteorSubscribe('users');
		$meteor.autorun($scope, function() {
			$scope.$meteorSubscribe('parties', 
				{
					skip: (parseInt($scope.getReactively('page')) - 1) * parseInt($scope.getReactively('perPage')),
					limit: parseInt($scope.getReactively('perPage')),
					sort: $scope.getReactively('sort')
				}, 
				$scope.getReactively('search')
			)
			.then(function() {
					$scope.partiesCount = $meteor.object(Counts ,'numberOfParties', false);
			});
		});
		
		$scope.remove = function(party){
			$scope.parties.remove(party);
		};

		$scope.removeAll = function(){
			$scope.parties.remove();
		};

		$scope.pageChanged = function(newPage) {
	  		$scope.page = newPage;
		};

		$scope.$watch('orderProperty', function(){
			if ($scope.orderProperty)
				$scope.sort = {name: parseInt($scope.orderProperty)};
		});

		$scope.getUserById = function(userId){
			return Meteor.users.findOne(userId);
		};

		$scope.creator = function(party){
			if (!party)
				return;
			var owner = $scope.getUserById(party.owner);
			if (!owner)
				return "nobody";

			if ($rootScope.currentUser)
				if ($rootScope.currentUser._id)
					if (owner._id === $rootScope.currentUser._id)
						return "me";
			return owner;
		};
		
		$scope.outstandingInvitations = function (party) {
			return _.filter($scope.users, function (user) {
				return (_.contains(party.invited, user._id) && !_.findWhere(party.rsvps, {user: user._id}));
			});
		};
	}
]);