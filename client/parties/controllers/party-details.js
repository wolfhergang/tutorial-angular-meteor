angular.module("socially").controller("PartyDetailsCtrl", ['$scope', '$stateParams', '$meteor',
	function($scope, $stateParams, $meteor){
		
		$scope.users = $meteor.collection(Meteor.users, false).subscribe('users');
		$scope.party = $meteor.object(Parties, $stateParams.partyId, false);
		$scope.$meteorSubscribe("parties");

		$scope.save = function() {
			$scope.party.save().then(
				function(numberOfDocs){
					console.log('save success doc affected ', numberOfDocs);
				}, 
				function(error){
					console.log('save error', error);
				}
			);
		};
		$scope.reset = function() {
			$scope.party.reset();
		};
		$scope.invite = function(user){
			$meteor.call('invite', $scope.party._id, user._id).then(
				function(data){
					console.log('success inviting', data);
				},
				function(err){
					console.log('failed', err);
				}
			);
		};
		$scope.rsvp = function(partyId, rsvp){
			$meteor.call('rsvp', partyId, rsvp)
			.then(
				function(data){
					console.log('success responding', data);
				},
				function(err){
					console.log('failed', err);
				}
			);
		};
	}
]);